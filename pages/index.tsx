import * as React from 'react'
import { NextPage } from 'next'

const Home: NextPage = () => <div>Hello World</div>

export default Home
